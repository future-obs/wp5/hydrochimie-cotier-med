
# HYDROMED

A Jupyter Notebook and Python Script to build a consolidated and qualified dataset with physico-chemical measurements on the surface of the North Western Mediterranean sea, taken from publicly available datasets.

The Markdown cells in the notebook detail where the original data is retrieved from and all the steps that perform the filtering, qualification and aggregation steps.
Some graphical and tabular output is produced allowing to check the overall validity of different steps.


[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:90e66cf5b570c6dc902dec4a52888a52d2b43d73/)](https://archive.softwareheritage.org/swh:1:dir:90e66cf5b570c6dc902dec4a52888a52d2b43d73;origin=https://gitlab.sb-roscoff.fr/future-obs/wp5/hydrochimie-cotier-med.git;visit=swh:1:snp:90e8a6623c1dca2a582a51dcee2776cb09a75453;anchor=swh:1:rev:4947a300101e5333e0e343ad8d3ef367e7b7f0ff)



## Installation

HYDROMED can be run either as an independent Python script or as a Jupyter Notebook. The [requirements.txt](requirements.txt) file provides the list of components that are needed to run the script or notebook.

    
## Usage/Examples

The HYDROMED notebook can be run "as is". When first run, it will download the two primary datasets, apply all the cleaning, filtering and qualification steps and generate an output file with the consolidated result table.

The Markdown documentation in the notebook gives a detailed description of every step.



## Authors

- [Mark HOEBEKE](https://orcid.org/0000-0001-6311-9752)
- [Aurélien SCHMITT](https://orcid.org/0009-0006-1844-2765)


## License

HYDROMED - Copyright (c) 2024 - CNRS, Sorbonne Université

HYDROMED is distributed under the terms of the [GNU GPL V3 License](LICENSE)



## Acknowledgements

Development of the HYDROMED Notebook was carried out in the framwork of the FUTURE-OBS project (ANR-22-POCE-0004)

